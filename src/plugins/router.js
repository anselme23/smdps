import Vue from 'vue'
import Router from 'vue-router'

import Welcome from '@/components/Welcome'
import Dash from '@/components/Dashboard'
import Login from '@/components/Login'
import NotFound from '@/components/NotFound'
import Services from '@/components/Services'
import CatholicMemberRegistration from '@/components/CatholicMemberRegistration'
import AllCatholics from '@/components/AllCatholics'
import Logout from '@/components/Logout'

Vue.use(Router)

const routes = [
    { path: '/',name: "Welcome", component: Welcome},
    { path: '/login',name: "Login", component: Login},
    { path: '/dashboard',name: "Dashboard", component: Dash},
    { path: '/services',name: "Services", component: Services},
    { path: '/services/rca-catholic-member', name: "CatholicMemberRegistration", component: CatholicMemberRegistration},
    { path: '/all-catholics', name: "All-catholics", component: AllCatholics},
    { path: '/logout', name: "Logout", component: Logout},
    { path: '*',name: "Not Found", component: NotFound}
]


export default new Router({
    routes,
    mode: 'history'
})
